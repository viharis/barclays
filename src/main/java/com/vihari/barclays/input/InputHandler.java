package com.vihari.barclays.input;

/**
 * 
 *
 * @author vihari
 */
public interface InputHandler {

    public void process() throws Exception;
}
