<<<<<<< HEAD
# barclays-assignment

Coding problem Airport Baggage - Pathfinding
 
Denver International Airport has decided to give an automated baggage system another shot. The hardware and tracking systems from the previous attempt are still in place, they just need a system to route the baggage.  The system will route baggage checked, connecting, and terminating in Denver.
 
You have been asked to implement a system that will route bags to their flights or the proper baggage claim.  The input describes the airport conveyor system, the departing flights, and the bags to be routed.  The output is the optimal routing to get bags to their destinations.  Bags with a flight id of “ARRIVAL” are terminating in Denver are routed to Baggage Claim.
 
Input: The input consists of several sections.  The beginning of each section is marked by a line starting: “# Section:”
Section 1: A weighted bi-directional graph describing the conveyor system.
Format: <Node 1> <Node 2> <travel_time>
Section 2: Departure list
           Format: <flight_id> <flight_gate> <destination> <flight_time>
Section 3: Bag list
           Format: <bag_number> <entry_point> <flight_id>
 
Output: The optimized route for each bag
<Bag_Number> <point_1> <point_2> [<point_3>, …] : <total_travel_time>
The output should be in the same order as the Bag list section of the input.
 
Example Input:

Section: Conveyor System

Concourse_A_Ticketing A5 5

A5 BaggageClaim 5

A5 A10 4

A5 A1 6

A1 A2 1

A2 A3 1

A3 A4 1

A10 A9 1

A9 A8 1

A8 A7 1

A7 A6 1

Section: Departures

UA10 A1 MIA 08:00

UA11 A1 LAX 09:00

UA12 A1 JFK 09:45

UA13 A2 JFK 08:30

UA14 A2 JFK 09:45

UA15 A2 JFK 10:00

UA16 A3 JFK 09:00

UA17 A4 MHT 09:15

UA18 A5 LAX 10:15

Section: Bags

0001 Concourse_A_Ticketing UA12

0002 A5 UA17

0003 A2 UA10

0004 A8 UA18

0005 A7 ARRIVAL

Example Output:

0001 Concourse_A_Ticketing A5 A1 : 11

0002 A5 A1 A2 A3 A4 : 9

0003 A2 A1 : 1

0004 A8 A9 A10 A5 : 6

0005 A7 A8 A9 A10 A5 BaggageClaim : 12
=======
**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
>>>>>>> branch 'master' of https://viharis@bitbucket.org/viharis/barclays.git
